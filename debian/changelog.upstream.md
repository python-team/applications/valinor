# valinor

## 1.1.0 - 2016-02-22

- IDE detection: add uvision5

## 0.0.14 - 2016-02-03

- progen data: remove default debugger

## 0.0.13 - 2016-01-14

- progen data: sources sorted by the file name
- progen data: build dir set to the generated dir

## 0.0.12 - 2015-12-14

- show full path to generated project files
- elf: catch ELFError - invalid elf file
- elf: fix logging import
- elf: get sources from executable
- IDE detection: use progendef for target manipulation

## 0.0.11 - 2015-10-15

- Compatibility and Usability Improvements
- now compatible with virtualenv on windows (which the yotta Windows installer
  uses)
- Improve error messages when no boards are connected

## 0.0.10 - 2015-10-12

- Improve Compatibility
- substantially improve CI setup
- fix python 3 compatibility
- don't use implicit relative imports (resolves issues running in virtualenv
  on Windows)

## 0.0.9 - 2015-09-08

- Fixes ARMmbed/yotta#389 (cannot start debugging in Windows)
- Update copyright to ARM

## 0.0.8 - 2015-07-31

- update to project-generator 0.7 (which includes major improvements in the
  project file generation infrastructure.)
