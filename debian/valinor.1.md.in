% VALINOR(1) __VERSION__ | User Commands
% 
% May 2019

# NAME

**valinor** - generate debugger project files, and launch a debugger, to debug an ELF file

# SYNOPSIS

| **valinor** \[**\-\-tool** _TOOL_\] \[**\-\-project\-dir** _PROJECT\_DIR_\] \[**\-\-no\-open**\] **\-\–target** _TARGET_ **executable**

| **valinor** **\–\-version**

# DESCRIPTION

**valinor** is designed to be used as a proxy debug command for **yotta** targets to provide as their scripts.debug command.

# OPTIONS

-t, \-\-tool _TOOL_
:   debug tool (IDE) to generate for (if omitted, a debug project will be generated for an IDE detected on your system, defaulting to opening a GDB debug session, if no known IDEs are detected)

-d, \-\-project\-dir _PROJECT_DIR_
:   directory in which to generate any necessary project files (default: directory of the executable argument)

-n, \-\-no\-open
:   do not open debug session (generate the necessary files to enable debugging and print command to proceed)

\-\–target _TARGET_
:   target board to generate project file for (e.g. K64F) (this name is passed to project\_generator - any name it accepts will work)

\-\-version
:   print version and exit

# YOTTA TARGET DEFINITIONS

To use valinor to add debug support to a yotta target description, add the following to your target.json file (replacing K64F with the project\_generator target ID for the chip/board on your target):

```
"scripts":{
    "debug": ["valinor", "--target", "frdm-k64f", "$program"]
}
```

# REPORTING BUGS

Upstream bug tracker: https://github.com/ARMmbed/valinor/issues

# COPYRIGHT

Copyright (c) 2014-2015 ARM Limited

# AUTHOR

This manual page is based on the valinor documentation. It was created by Nick Morrott <nickm@debian.org> for the Debian GNU/Linux system, but may be used by others

# SEE ALSO

**yotta**(1), **progen**(1), **progendef**(1)
